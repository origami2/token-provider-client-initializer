var assert = require('assert');
var TokenProviderClientInitializer = require('..');
var EventEmitter = require('events').EventEmitter;

describe('TokenProviderClientInitializer', function () {
  it('requires a resolution function', function () {
    assert.throws(
      function () {
        new TokenProviderClientInitializer();
      },
      /resolution function is required/
    );
  });
  
  it('returns a function', function () {
    assert.equal('function', typeof(new TokenProviderClientInitializer(function () {})));
  });
  
  it('function resolves', function (done) {
    var fakeSocket = new EventEmitter();
    
    (new TokenProviderClientInitializer(function () {}))(fakeSocket)
    .then(function () {
      done();
    });
  });
  
  it('function resolves a ready function', function (done) {
    var fakeSocket = new EventEmitter();
    
    (new TokenProviderClientInitializer(function () {}))(fakeSocket)
    .then(function (ready) {
      try {
        assert.equal('function', typeof(ready));
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('when the function is ready, it emits token-providers', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      try {
        assert.equal('function', typeof(callback));
        
        done();
      } catch (e) {
        done(e);
      }
    });
    
    (new TokenProviderClientInitializer(function () {}))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('when describe-token-providers callback is called successfully, resolves a client builder function', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            methods: {
              'echo': [ 'what' ]
            }
          }
        }
      );
    });
    
    (new TokenProviderClientInitializer(
      function (clientBuilder) {
        try {
          assert.equal('function', typeof(clientBuilder));
        
          done();
        } catch (e) {
          done(e);
        }
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('when client builder is invoked, a Client is created', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            methods: {
              'echo': [ 'what' ]
            }
          }
        }
      );
    });
    
    (new TokenProviderClientInitializer(
      function (clientBuilder) {
        try {
          var client = clientBuilder();
        
          assert(client);
          assert(client.StubPlugin1);
          assert(client.StubPlugin1.echo);
          
          done();
        } catch (e) {
          done(e);
        }
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('when client method is invoked get-token message is sent', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            methods: {
              'echo': [ 'what' ]
            }
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'get-token', 
      function (stackToken, pluginName, methodName, params, callback) {
        try {
          assert(!stackToken);
          assert.equal('StubPlugin1', pluginName);
          assert.equal('echo', methodName);
          assert.deepEqual(
            {
              what: 'hello',
            },
            params
          );
          assert.equal('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    (new TokenProviderClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder();
        
        client.StubPlugin1.echo('hello');
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('stackToken is preserved', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            methods: {
              'echo': [ 'what' ]
            }
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'get-token', 
      function (stackToken, pluginName, methodName, params, callback) {
        try {
          assert.deepEqual({some: 'token' }, stackToken);
          assert.equal('StubPlugin1', pluginName);
          assert.equal('echo', methodName);
          assert.deepEqual(
            {
              what: 'hello',
            },
            params
          );
          assert.equal('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    (new TokenProviderClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder({some:'token'});
        
        client.StubPlugin1.echo('hello');
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('resolves promise when token-providers callback is invoked with result', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            methods: {
              'echo': [ 'what' ]
            }
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'get-token', 
      function (stackToken, pluginName, methodName, params, callback) {
        callback(null, 'hello');
      }
    );
    
    (new TokenProviderClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder();
        
        client
        .StubPlugin1.echo('hello')
        .then(function (result) {
          try {
            assert.equal('hello', result);
            
            done();
          } catch (e) {
            done(e);
          }
        });
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('resolves promise when get-token callback is invoked with error', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-token-providers', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            methods: {
              'echo': [ 'what' ]
            }
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'get-token', 
      function (stackToken, pluginName, methodName, params, callback) {
        callback('my error');
      }
    );
    
    (new TokenProviderClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder();
        
        client.StubPlugin1.echo('hello')
        .catch(function (error) {
          try {
            assert.equal('my error', error);
            
            done();
          } catch (e) {
            done(e);
          }
        });
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
});