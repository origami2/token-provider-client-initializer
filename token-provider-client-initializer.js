var TokenProviderClient = require('origami-token-provider-client');

function TokenProviderClientInitializer(resolution) {
  // Token Provider Client Initializer
  
  if (!resolution) throw new Error('resolution function is required');
  
  return function (socket, namespace, crane) {
    // Token Provider Client Initializer
  
    socket
    .emit(
      'describe-token-providers',
      function (err, tokenProviders) {
        var apis = {};
        
        for (var api in tokenProviders) {
          apis[api] = tokenProviders[api].methods;
        }
        
        resolution(function (token) {
          return new TokenProviderClient(socket, apis, token);
        });
      }
    );

    return Promise.resolve(function () {
      // ready function
    });
  };
}

module.exports = TokenProviderClientInitializer;